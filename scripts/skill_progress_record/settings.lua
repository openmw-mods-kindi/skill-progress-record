local I = require("openmw.interfaces")
local modInfo = require("scripts.skill_progress_record.modInfo")
local core = require("openmw.core")
local ui = require("openmw.ui")
local util = require("openmw.util")
local async = require("openmw.async")
local input = require("openmw.input")
local storage = require("openmw.storage")
local menu = require("openmw.menu")
local playerSect = storage.playerSection("Settings_SkillProgressRecord_CONTROLS")

local isWaitingKeybind = false
local optionKey = nil
local keysetsSaved = {}

local cmds = {
    ["Open Record"] = "SkillProgressRecord_OpenRecord",
    ["Open Reset"] = "SkillProgressRecord_OpenReset",
    ["Yes"] = "SkillProgressRecord_Yes",
    ["No"] = "SkillProgressRecord_No"
}

I.Settings.registerPage {
    key = "SkillProgressRecord_eqnx",
    l10n = "skill_progress_record",
    name = "settings_modName",
    description = core.l10n("skill_progress_record")("settings_modDesc"):format(modInfo.MOD_VERSION)
}

I.Settings.registerRenderer("kuyondo_keybind", function(value, set, arg)
    local label = type(value) == "userdata" and value.label or input.getKeyName(value) -- gila betul kerja
    return {
        template = I.MWUI.templates.bordersThick,
        props = {
            size = util.vector2(128, 32)
        },
        content = ui.content {{
            template = I.MWUI.templates.padding,
            content = ui.content {{
                template = I.MWUI.templates.padding,
                content = ui.content {{
                    template = I.MWUI.templates.textNormal,
                    props = {
                        text = label
                    }
                }}
            }}
        }},
        events = {
            mouseClick = async:callback(function(e)
                ui.showMessage("Choose a key or combination to bind. Release to confirm.")
                isWaitingKeybind = true
                optionKey = arg
                keysetsSaved = {}
            end)
        }
    }
end)

I.Settings.registerGroup {
    key = "Settings_SkillProgressRecord_CONTROLS",
    page = "SkillProgressRecord_eqnx",
    l10n = "skill_progress_record",
    name = "setings_modCategory1_name",
    description = "setings_modCategory1_desc",
    permanentStorage = true,
    settings = {{
        key = "Open Record",
        renderer = "kuyondo_keybind",
        name = "setings_modCategory1_setting1_name",
        description = "setings_modCategory1_setting1_desc",
        default = input.KEY.Z,
        argument = "Open Record"
    }, {
        key = "Open Reset",
        renderer = "kuyondo_keybind",
        name = "setings_modCategory1_setting6_name",
        description = "setings_modCategory1_setting6_desc",
        default = input.KEY.R,
        argument = "Open Reset"
    }, {
        key = "Yes",
        renderer = "kuyondo_keybind",
        name = "setings_modCategory1_setting7_name",
        description = "setings_modCategory1_setting7_desc",
        default = input.KEY.Y,
        argument = "Yes"
    }, {
        key = "No",
        renderer = "kuyondo_keybind",
        name = "setings_modCategory1_setting8_name",
        description = "setings_modCategory1_setting8_desc",
        default = input.KEY.N,
        argument = "No"
    }, {
        key = "textSize",
        renderer = "number",
        name = "setings_modCategory1_setting9_name",
        description = "setings_modCategory1_setting9_desc",
        default = 16
    }}
}

local function getKeyPressMatch(key)
    for optionName, keysets in pairs(playerSect:asTable()) do
        local firstKey = type(keysets) == "table" and keysets.key1 or keysets
        local secondKey = type(keysets) == "table" and keysets.key2
        if input.isKeyPressed(firstKey) then
            if secondKey then
                if secondKey == key.code then
                    return optionName
                end
            else
                return optionName
            end
        end
    end
end

return {
    engineHandlers = {
        onKeyRelease = function(key)
            isWaitingKeybind = false
            keysetsSaved = {}
        end,
        onKeyPress = function(key)
            if key.code == input.KEY.Escape then
                isWaitingKeybind = false
                return
            end

            if isWaitingKeybind then
                if keysetsSaved.firstKey then
                    keysetsSaved.secondKey = key.code
                else
                    keysetsSaved.firstKey = key.code
                    keysetsSaved.secondKey = nil
                end

                local newLabel = input.getKeyName(keysetsSaved.firstKey)
                if keysetsSaved.secondKey then
                    newLabel = newLabel .. " + " .. input.getKeyName(keysetsSaved.secondKey)
                end

                playerSect:set(optionKey, {
                    label = newLabel,
                    key1 = keysetsSaved.firstKey,
                    key2 = keysetsSaved.secondKey
                })

                return
            end

            if menu.getState() == menu.STATE.NoGame then
                return
            end

            local optionKeyMatched = getKeyPressMatch(key)
            if optionKeyMatched then
                input.activateTrigger(cmds[optionKeyMatched])
            end

        end
    }
}
