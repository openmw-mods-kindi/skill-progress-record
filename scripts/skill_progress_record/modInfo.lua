local CHANGES = [[
    Change hotkey options from typing a letter to a keybind
]]

return setmetatable({
    MOD_NAME = "Skill Progress Record",
    MOD_VERSION = 0.3,
    MIN_API = 60,
    CHANGES = CHANGES
}, {
    __tostring = function(modInfo)
        return string.format("\n[%s]\nVersion: %s\nMinimum API: %s\nChanges: %s", modInfo.MOD_NAME, modInfo.MOD_VERSION,
            modInfo.MIN_API, modInfo.CHANGES)
    end,
    __metatable = tostring
})

-- require("scripts.skill_progress_record.modInfo")
